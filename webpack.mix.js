const mix = require('laravel-mix');
mix.options({
	extractVueStyles: false,
	processCssUrls: true,
	terser: {},
	purifyCss: true,
	//purifyCss: {},
	postCss: [require('autoprefixer')],
	clearConsole: false,
	cssNano: {
		discardComments: {removeAll: true},
	}
});

mix
	.js('resources/js/app.js', 'public/js')
	.sass('resources/sass/app.scss', 'public/css');

mix
	.styles([
		'resources/css/klorofil/bootstrap.css',
		'resources/css/klorofil/font-awesome.css',
		'resources/css/klorofil/style.css',
		'resources/css/klorofil/main.css',
		'resources/css/klorofil/demo.css'
	],'public/css/eFrd5.css')
	.styles([
		'resources/css/klorofil/datatables/dataTables.bootstrap.css'
	],'public/css/dt.css')
	.styles([
		'node_modules/selectize/dist/css/selectize.css'
	], 'public/css/s2.css')
	.copy('resources/css/klorofil/fonts-awesome','public/fonts')
	.copy('resources/css/klorofil/fonts-linear','public/css/fonts')
	.copy('resources/css/klorofil/fonts-awesome','public/fonts')
	.scripts([
		'resources/js/klorofil/jquery.js',
		'resources/js/klorofil/bootstrap.js',
		'resources/js/klorofil/jquery.slimscroll.js',
		'resources/js/klorofil/klorofil-common.js'
	],'public/js/eFrd5.js')
	.scripts([
		'resources/js/klorofil/datatables/jquery.dataTables.js',
		'resources/js/klorofil/datatables/dataTables.bootstrap.js'
	], 'public/js/dt.js')
	.scripts([
		'node_modules/selectize/dist/js/standalone/selectize.js'
	], 'public/js/s2.js');