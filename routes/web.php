<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// start is admin
Route::prefix('admin')->name('admin.')->middleware(['is_admin'])->namespace('Admin')->group(function(){
	Route::get('dashboard', 'HomeController@adminHome')->name('home');
	Route::resource('servers','ServersController')->only(['index','store','destroy','update']);
	Route::resource('domains','DomainsController')->only(['index','store','destroy','update']);
	Route::resource('owners','OwnersController')->only(['index','store','destroy','update']);
	Route::get('runcloud','RuncloudController@index');
});
// end is admin

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
