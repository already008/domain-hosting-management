<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Servers;
use Illuminate\Http\Request;

class ServersController extends Controller
{
	public function index()
	{
		$servers = Servers::all();
		return view('admin.servers.index', compact('servers'));
	}

	public function store(Request $request)
	{
		$request->validate([
			'name_server' 	=> 'required|unique:servers',
			'ip_address' 	=> 'required|unique:servers'
		]);
		Servers::create($request->all());
		return redirect()->back()->with('success','Input Data Server Successfully');
	}

	public function update(Request $request, $id)
	{
		$server = Servers::find($id);
		$server->update($request->all());
		return redirect()->back()->with('success','Update Data Server Successfully');
	}

	public function destroy($id)
	{
		Servers::find($id)->delete();
		return redirect()->back()->with('success','Delete Data Server Successfully');
	}
}
