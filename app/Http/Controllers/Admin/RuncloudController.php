<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class RuncloudController extends Controller
{
    public function index()
    {
    	$client = new Client([
    		'base_uri' => 'https://manage.runcloud.io/api/v2/',
    	]);
    	$a = $client->request('GET', '/get', ['auth' => ['rkd5LxNE56wVlMVHnWxIpX05PXyxrQhArtHdjuVlurZh', '4fCzVvFO6X4g7wuj4csrE0YuN5LEr5cGpxOULErEs5nxkwtys6cUp5Nekx584HGD']]);
    	dd($a);
    }
}
