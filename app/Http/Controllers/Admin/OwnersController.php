<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class OwnersController extends Controller
{
	public function index()
	{
		$owners = User::all();
		return view('admin.owners.index', compact('owners'));
	}

	public function store(Request $request)
	{
		$request->validate([
			'name'		=> 'required|unique:users',
			'email'		=> 'required|email|unique:users',
			'password'	=> 'required|min:8',
			'no_hp'		=> 'required|min:10|max:13',
		]);
		$owner 						= User::create($request->all());
		$owner->password 			= Hash::make($request->password);
		$owner->is_admin 			= '0';
		$owner->remember_token 		= Str::random(15);
		$owner->email_verified_at 	= now();
		$owner->save();
		return redirect()->back()->with('success','Input Data Owner Successfully');
	}

	public function update(Request $request, $id)
	{
		$owner = User::find($id);
		if(is_null($request->password)){
			$owner->password 	= $owner->password;
		}else{
			$owner->password 	= Hash::make($request->password);
		}
		$owner->name = $request->name;
		$owner->email = $request->email;
		$owner->is_admin = $owner->is_admin;
		$owner->no_hp = $request->no_hp;
		$owner->save();
		return redirect()->back()->with('success','Update Data Owner Successfully');
	}

	public function destroy($id)
	{
		User::find($id)->delete();
		return redirect()->back()->with('success','Delete Data Owner Successfully');
	}
}
