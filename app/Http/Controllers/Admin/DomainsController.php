<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Servers;
use App\Models\Domains;
use App\User;
use Illuminate\Http\Request;

class DomainsController extends Controller
{
	public function index()
	{
		$domains 	= Domains::with(['server','user'])->get();
		// dd($domains);
		$servers 	= Servers::pluck('name_server','id');
		$owners 	= User::pluck('name','id');
		return view('admin.domains.index', compact('domains','servers','owners'));
	}

	public function store(Request $request)
	{
		$request->validate([
			'domain_name' 		=> 'required|unique:domains',
			'domain_expired' 	=> 'required',
			'server_id' 		=> 'required',
			'user_id' 			=> 'required'
		]);
		Domains::create($request->all());
		return redirect()->back()->with('success','Input Data Domain Successfully');
	}

	public function update(Request $request, $id)
	{
		$domain = Domains::find($id);
		$domain->update($request->all());
		return redirect()->back()->with('success','Update Data Domain Successfully');
	}

	public function destroy($id)
	{
		Domains::find($id)->delete();
		return redirect()->back()->with('success','Delete Data Domain Successfully');
	}
}
