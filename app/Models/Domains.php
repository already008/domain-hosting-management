<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domains extends Model
{
	protected $table 	= 'domains';
	protected $fillable = ['domain_name','domain_expired','server_id','user_id'];

	public function server()
	{
		return $this->belongsTo('App\Models\Servers');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
