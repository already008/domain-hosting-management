<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Servers extends Model
{
	protected $table		= 'servers';
	protected $fillable 	= ['name_server','ip_address'];

	public function domains(){
		return $this->hasMany('App\Models\Domains');
	}
}