<!doctype html>
<html lang="en">

<head>
	<title>@yield('app') | {{ config('app.name') }}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	{{-- <link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css"> --}}
	<link rel="stylesheet" href="{{ asset('css/eFrd5.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	@stack('styles')
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		@include('admin.layouts.navbar')
		@include('admin.layouts.sidebar')
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				@yield('content')
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		@include('admin.layouts.footer')
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="{{ asset('js/eFrd5.js') }}"></script>
	@stack('scripts')
	{{-- <script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js"></script> --}}
</body>

</html>
