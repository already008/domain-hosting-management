<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li><a href="{{ route('admin.home') }}" class="{{ (request()->is('admin/dashboard')) ? 'active' : '' }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
				<li><a href="{{ route('admin.servers.index') }}" class="{{ (request()->is('admin/servers')) ? 'active' : '' }}"><i class="lnr lnr-rocket"></i> <span>Servers</span></a></li>
				<li><a href="{{ route('admin.domains.index') }}" class="{{ (request()->is('admin/domains')) ? 'active' : '' }}"><i class="lnr lnr-link"></i> <span>Domains</span></a></li>
				<li><a href="{{ route('admin.owners.index') }}" class="{{ (request()->is('admin/owners')) ? 'active' : '' }}"><i class="lnr lnr-users"></i> <span>Owners</span></a></li>
			</ul>
		</nav>
	</div>
</div>
<!-- END LEFT SIDEBAR -->