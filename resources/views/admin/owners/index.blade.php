@extends('admin.layouts.app')

@section('app')
Owners
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<!-- RECENT PURCHASES -->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Owners</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped" id="owners-table" style="width:100%">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Lengkap</th>
								<th>Email</th>
								<th>No HP</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@php
							$no = 1;
							@endphp
							@foreach($owners as $ow)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $ow->name }}</td>
								<td>{{ $ow->email }}</td>
								<td>{{ $ow->no_hp }}</td>
								<td>
									@if($ow->is_admin == 1)
									<span class="label label-danger">Admin</span>
									@else
									<span class="label label-info">Site Owner</span>
									@endif
								</td>
								<td>@include('admin.owners.edit_delete')</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							@if ($message = Session::get('success'))
								<div class="alert alert-success alert-dismissible" role="alert">
									<i class="fa fa-check-circle"></i>{{ $message }}
								</div>
							@endif
							@if ($errors->any())
							<div class="alert alert-danger alert-dismissible" role="alert">
								@foreach ($errors->all() as $error)
									<i class="fa fa-times-circle"></i>{{ $error }}<br/>
								@endforeach
							</div>
							@endif
						</div>

						<div class="col-md-6 text-right">
							@include('admin.owners.form_add')
						</div>
					</div>
				</div>
			</div>
			<!-- END RECENT PURCHASES -->
		</div>
	</div>
</div>
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/dt.css') }}">
@endpush

@push('scripts')
	<script src="{{ asset('js/dt.js') }}"></script>
	<script>
		$(document).ready(function() {
			$('#owners-table').DataTable( {
				"order": [[ 0, "desc" ]]
			});
		} );
	</script>
@endpush