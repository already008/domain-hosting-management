<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddOwner">Add Data Owner</a>

<div class="modal fade" id="modalAddOwner" tabindex="-1" role="dialog" aria-labelledby="modalAddOwnerLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content text-left">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalAddOwnerLabel">Add Data</h4>
			</div>
			
			{{ Form::open(['route'=>'admin.owners.store']) }}
			<div class="modal-body">
				{{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Nama Lengkap']) }}
				<br>
				{{ Form::number('no_hp',old('no_hp'),['class'=>'form-control','placeholder'=>'No Hp']) }}
				<br>
				{{ Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'Email']) }}
				<br>
				{{ Form::password('password',['class'=>'form-control','placeholder'=>'Password']) }}
				<br>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
			{{ Form::close() }}

		</div>
	</div>
</div>