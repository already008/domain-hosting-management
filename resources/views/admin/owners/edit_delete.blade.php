<div class="demo-button">
	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_{{ $ow->id }}">Edit</button>
	{{ Form::open(['route'=>['admin.owners.destroy',$ow->id]]) }}
	@method('DELETE')
	@if($ow->id == 1)
	<button type="button" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
	@else
	<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
	@endif
	{{ Form::close() }}
</div>

<!-- Modal -->
<div class="modal fade" id="edit_{{ $ow->id }}" tabindex="-1" role="dialog" aria-labelledby="edit_{{ $ow->id }}Label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="edit_{{ $ow->id }}Label">Edit {{ $ow->nama_lengkap }}</h4>
			</div>
			{{ Form::model($ow, ['route' => ['admin.owners.update', $ow->id]]) }}
			@method('PATCH')
			<div class="modal-body">
				{{ Form::text('name',$ow->name,['class'=>'form-control','placeholder'=>'Nama Lengkap','style'=>'width: 100%;']) }}
				<br><br>
				{{ Form::number('no_hp',$ow->no_hp,['class'=>'form-control','placeholder'=>'No Hp','style'=>'width: 100%;']) }}
				<br><br>
				{{ Form::email('email',$ow->email,['class'=>'form-control','placeholder'=>'Email','style'=>'width: 100%;']) }}
				<br><br>
				{{ Form::password('password',['class'=>'form-control','placeholder'=>'Password','style'=>'width: 100%;']) }}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>