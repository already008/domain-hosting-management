<div class="demo-button">
	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_{{ $d->id }}">Edit</button>
	{{ Form::open(['route'=>['admin.domains.destroy',$d->id]]) }}
	@method('DELETE')
	<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
	{{ Form::close() }}
</div>

<!-- Modal -->
<div class="modal fade" id="edit_{{ $d->id }}" tabindex="-1" role="dialog" aria-labelledby="edit_{{ $d->id }}Label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="edit_{{ $d->id }}Label">Edit {{ $d->name_server }}</h4>
			</div>
			{{ Form::model($d, ['route' => ['admin.domains.update', $d->id]]) }}
			@method('PATCH')
			<div class="modal-body">
				{{ Form::text('domain_name',$d->domain_name,['class'=>'form-control','placeholder'=>'Domain Name','id'=>'domain_name','style'=>'width: 100%;']) }}
				<br>
				<br>
				{{ Form::text('domain_expired',$d->domain_expired,['class'=>'form-control domain_expired','placeholder'=>'Domain Expired','id'=>'domain_expired','style'=>'width: 100%;']) }}
				<br>
				<br>
				{{ Form::select('server_id', $servers, $d->server_id, ['placeholder' => 'Select a Servers...','id'=>'server_id','style'=>'width: 100%;']) }}
				<br>
				<br>
				{{ Form::select('user_id', $owners, $d->user_id, ['placeholder' => 'Select a Owners...','id'=>'owner_id','style'=>'width: 100%;']) }}
				<br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>