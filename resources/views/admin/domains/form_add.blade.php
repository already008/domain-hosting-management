<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddDomains">Add Data Domains</a>

<div class="modal fade" id="modalAddDomains" tabindex="-1" role="dialog" aria-labelledby="modalAddDomainsLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content text-left">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalAddDomainsLabel">Add Data</h4>
			</div>
			
			{{ Form::open(['route'=>'admin.domains.store']) }}
			<div class="modal-body">

				{{ Form::text('domain_name',old('domain_name'),['class'=>'form-control','placeholder'=>'Domain Name','id'=>'domain_name']) }}
				<br>
				{{ Form::text('domain_expired',old('domain_expired'),['class'=>'form-control','placeholder'=>'Domain Expired','id'=>'domain_expired']) }}
				<br>
				{{ Form::select('server_id', $servers, null, ['placeholder' => 'Select a Servers...','id'=>'server_id']) }}
				<br>
				{{ Form::select('user_id', $owners, null, ['placeholder' => 'Select a Owners...','id'=>'owner_id']) }}
				<br>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
			{{ Form::close() }}

		</div>
	</div>
</div>