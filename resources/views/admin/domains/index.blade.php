@extends('admin.layouts.app')

@section('app')
Domains
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<!-- RECENT PURCHASES -->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Domains</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped" id="domains-table" style="width:100%">
						<thead>
							<tr>
								<th>No.</th>
								<th>Domain Name</th>
								<th>Domain Expired</th>
								<th>Server Name</th>
								<th>Owner Name</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@php
							$no = 1;
							@endphp
							@foreach($domains as $d)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $d->domain_name }}</td>
								<td>{{ $d->domain_expired }}</td>
								<td>{{ $d->server->name_server }}</td>
								<td>{{ $d->user->name }}</td>
								<td>@include('admin.domains.edit_delete')</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							@if ($message = Session::get('success'))
								<div class="alert alert-success alert-dismissible" role="alert">
									<i class="fa fa-check-circle"></i>{{ $message }}
								</div>
							@endif
							@if ($errors->any())
							<div class="alert alert-danger alert-dismissible" role="alert">
								@foreach ($errors->all() as $error)
									<i class="fa fa-times-circle"></i>{{ $error }}<br/>
								@endforeach
							</div>
							@endif
						</div>

						<div class="col-md-6 text-right">
							@include('admin.domains.form_add')
						</div>
					</div>
				</div>
			</div>
			<!-- END RECENT PURCHASES -->
		</div>
	</div>
</div>
@endsection

@push('styles')
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	<link rel="stylesheet" href="{{ asset('css/dt.css') }}">
	<link rel="stylesheet" href="{{ asset('css/s2.css') }}">
	<style>
		input.form-control.domain_expired.form-control.input {
			width: 100%;
		}
	</style>	
@endpush

@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
	<script src="{{ asset('js/dt.js') }}"></script>
	<script src="{{ asset('js/s2.js') }}"></script>
	<script>
		flatpickr("#domain_expired",{
			altInput: true,
			altFormat: "F j, Y",
			dateFormat: "Y-m-d",
		});
		$(document).ready(function() {
			$('#domains-table').DataTable( {
				"order": [[ 1, "asc" ]]
			});
		} );
		$('#server_id').selectize();
		$('#owner_id').selectize();
	</script>
@endpush