<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddServer">Add Data Server</a>

<div class="modal fade" id="modalAddServer" tabindex="-1" role="dialog" aria-labelledby="modalAddServerLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content text-left">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modalAddServerLabel">Add Data</h4>
			</div>
			
			{{ Form::open(['route'=>'admin.servers.store']) }}
			<div class="modal-body">
				{{ Form::text('name_server',old('name_server'),['class'=>'form-control','placeholder'=>'Name Server']) }}
				<br>
				{{ Form::text('ip_address',old('ip_address'),['class'=>'form-control','placeholder'=>'IP Address']) }}
				<br>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
			{{ Form::close() }}

		</div>
	</div>
</div>