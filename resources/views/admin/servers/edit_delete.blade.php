<div class="demo-button">
	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_{{ $s->id }}">Edit</button>
	{{ Form::open(['route'=>['admin.servers.destroy',$s->id]]) }}
	@method('DELETE')
	<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
	{{ Form::close() }}
</div>

<!-- Modal -->
<div class="modal fade" id="edit_{{ $s->id }}" tabindex="-1" role="dialog" aria-labelledby="edit_{{ $s->id }}Label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="edit_{{ $s->id }}Label">Edit {{ $s->name_server }}</h4>
			</div>
			{{ Form::model($s, ['route' => ['admin.servers.update', $s->id]]) }}
			@method('PATCH')
			<div class="modal-body">
				<br>
				{{ Form::text('name_server',$s->name_server,['class'=>'form-control','placeholder'=>'Name Server','style'=>'width: 100%;']) }}
				<br><br>
				{{ Form::text('ip_address',$s->ip_address,['class'=>'form-control','placeholder'=>'IP Address','style'=>'width: 100%;']) }}
				<br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>