@extends('admin.layouts.app')

@section('app')
Servers
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<!-- RECENT PURCHASES -->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Servers</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-striped" id="servers-table" style="width:100%">
						<thead>
							<tr>
								<th>No.</th>
								<th>Name Server</th>
								<th>IP Address</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@php
							$no = 1;
							@endphp
							@foreach($servers as $s)
							<tr>
								<td>{{ $no++ }}</td>
								<td>{{ $s->name_server }}</td>
								<td>{{ $s->ip_address }}</td>
								<td>@include('admin.servers.edit_delete')</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-6">
							@if ($message = Session::get('success'))
								<div class="alert alert-success alert-dismissible" role="alert">
									<i class="fa fa-check-circle"></i>{{ $message }}
								</div>
							@endif
							@if ($errors->any())
							<div class="alert alert-danger alert-dismissible" role="alert">
								@foreach ($errors->all() as $error)
									<i class="fa fa-times-circle"></i>{{ $error }}<br/>
								@endforeach
							</div>
							@endif
						</div>

						<div class="col-md-6 text-right">
							@include('admin.servers.form_add')
						</div>
					</div>
				</div>
			</div>
			<!-- END RECENT PURCHASES -->
		</div>
	</div>
</div>
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/dt.css') }}">
@endpush

@push('scripts')
	<script src="{{ asset('js/dt.js') }}"></script>
	<script>
		$(document).ready(function() {
			$('#servers-table').DataTable( {
				"order": [[ 1, "desc" ]]
			});
		} );
	</script>
@endpush