<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = [
            [
				'name'				=> 'Developer Argia',
				'email'				=> 'developer@argiacyber.com',
				'is_admin'			=> '1',
				'password'			=> Hash::make('D2Vq7rj86rwM'),
				'remember_token' 	=> Str::random(15),
				'email_verified_at' => now(),
            ],
            [
				'name'				=>'User',
				'email'				=>'user@argia.co.id',
				'is_admin'			=>'0',
				'password'			=> bcrypt('V2mPM5VKHnPy'),
				'remember_token' 	=> Str::random(15),
				'email_verified_at' => now(),
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }
	}
}
