## Step 1
```bash
git clone git@gitlab.com:mrijalul/domain-hosting-management.git
```

## Step 2
```bash
composer install

npm install

cp .env.example .env
```

## Step 3
```bash
*setting .env
LOG_CHANNEL=daily
DB_DATABASE=namadatabaseyangsudahkamubuat
DB_USERNAME=userdefault
DB_PASSWORD=
```

## Step 4
```bash
php artisan migrate
```

# Step 5
```bash
npm run prod
```

## import seeder
```bash
php artisan db:seed --class=userSeeder
```